class ProductModel:
    id: int
    product_name: str
    product_description: str
    product_price: int
    category_name: str
    product_rating: int
    product_reviews: list
