class OrderModel:

    id: int
    order_price: int
    products: list
    purchase_date: str
    buyer_id: int
    discount: int
    total_tax: int
