from businesslogic.user import User
from persistence.models.order import OrderModel


class PersistenceUser(User):

    def set_orders(self, orders: OrderModel):
        self.orders = orders

    def get_orders(self):
        return self.orders
