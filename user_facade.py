from persistence.dao.user_dao import UserDao


class UserFacade:

    user_dao: UserDao

    def get_discount_for_user_by_id(self, user_id: int):
        user_model = self.user_dao.get_user_by_id(user_id)
        return self.user.calculate_discount_for_user(user_model)

    def set_user_dao(self, user_dao):
        self.user_dao = user_dao