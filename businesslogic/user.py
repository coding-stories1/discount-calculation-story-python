from abc import ABC

from persistence.models.user import UserModel


class User(ABC):

    LEVEL1_DISCOUNT = 0.05
    LEVEL2_DISCOUNT = 0.1
    LEVEL3_DISCOUNT = 0.15
    DISCOUNT_LEVEL1_THRESHOLD_USD = 1000
    DISCOUNT_LEVEL2_THRESHOLD_USD = 3000
    DISCOUNT_LEVEL3_THRESHOLD_USD = 5000
    discount_threshold_percentage_map: dict

    def __init__(self):
        self.discount_threshold_percentage_map = {
            self.DISCOUNT_LEVEL3_THRESHOLD_USD: self.LEVEL3_DISCOUNT,
            self.DISCOUNT_LEVEL2_THRESHOLD_USD: self.LEVEL2_DISCOUNT,
            self.DISCOUNT_LEVEL1_THRESHOLD_USD: self.LEVEL1_DISCOUNT,
        }

    def calculate_discount_for_user(self, user_model: UserModel):
        total_price_of_all_orders = self.calculate_total_price_of_all_orders(user_model)
        for discount, percent in self.discount_threshold_percentage_map.items():
            if total_price_of_all_orders >= discount:
                return percent
        return 0

    def calculate_total_price_of_all_orders(self, user_model: UserModel):
        return sum([order.get_gross_order_price() for order in user_model.get_orders()])
